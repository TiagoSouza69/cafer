# Correção para esta versão do coffee:coffee:

* Preenchido todo o espaço do header e do footer
* consertado o menu hamburger com os spans e a classe header_menu-icon: na linha 18 até a 21
* Organizado o Head como proposto
* Ícones das redes sociais reajustados com sucesso
* O mapa do modelo do Figma somente possui um lado arredondado